﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snd_FX : MonoBehaviour
{
	private AudioSource source;
	private bool changeMusic = false;
	private AudioClip musicClip;

	public void Play(AudioClip clip)
	{
		source = GetComponent<AudioSource>();
		source.clip = clip;
		source.Play();
	}

	public void ChangeMusic(AudioClip clip)
	{
		source = GetComponent<AudioSource>();
		musicClip = clip;
		changeMusic = true;
	}

	void Update()
	{
		if(null == musicClip && null != source && false == source.isPlaying)
		{
			Destroy(gameObject);
		}

		if(changeMusic)
		{
			//Si la canción está sonando
				if(true == source.isPlaying && musicClip != source.clip)
				{
					//Si el volumen es > 0
						if(0.0f < source.volume)
						{
							//Bajamos el audio
								source.volume -= Time.deltaTime;
						}
					//Si no
						else
						{
							//La paramos
								source.Stop();
						}
				}
			//Si no
				else
				{
					//Si el clip no está asignado
						if(musicClip != source.clip)
						{
							//Lo asignamos
								source.clip = musicClip;
								source.Play();
						}
					//Si sí está asignado
						else
						{
							//Si el volumen no está a tope
								if(1.0f > source.volume)
								{
									//Subimos el volumen
										source.volume += Time.deltaTime;
								}
							//Si sí lo está
								else
								{
									//Desactivamos el cambio de música
										changeMusic = false;
								}
						}
				}
		}
	}
}

public class SoundManager
{
	private static Snd_FX instance;

	public static void PlayFX(params string[] clips)
	{
		int index = Random.Range(0, clips.Length);
		PlayFX(clips[index]);

	}

	public static void PlayFX(string clip)
	{
		GameObject sndFx = new GameObject();
		sndFx.AddComponent<AudioSource>();
		sndFx.AddComponent<Snd_FX>();
		sndFx.transform.name = "SndFx";

		sndFx.GetComponent<Snd_FX>().Play(Resources.Load<AudioClip>("Audio/" + clip));
	}

	public static void PlaySong(string song)
	{
		if(null == instance)
		{
			GameObject sndFX = new GameObject();
			sndFX.AddComponent<AudioSource>();
			sndFX.AddComponent<Snd_FX>();
			sndFX.transform.name = "SndSong";
			Object.DontDestroyOnLoad(sndFX);

			sndFX.GetComponent<AudioSource>().loop = true;
			instance = sndFX.GetComponent<Snd_FX>();
		}

		AudioClip targetFile = Resources.Load<AudioClip>("Audio/"+song);
		instance.ChangeMusic(targetFile);
	}
}
