﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBullet : MonoBehaviour
{
    public string faction;
    public float speed = 10.0f;
    public float life = 1.0f;
    private float live = 0.0f;
    public int damage = 1;

    private Vector3 startingPos;

    void Start()
    {
      startingPos = transform.position;
    }

    void Update()
    {
        if(Vector3.Distance(startingPos, transform.position) > 1 && GetComponent<Renderer>().isVisible != true)
        {
          GetComponent<Renderer>().enabled = true;
        }

        transform.Translate(Vector3.up * Time.deltaTime * speed);
        live += Time.deltaTime;

        if(live > life)
        {
          Destroy(gameObject);
        }
    }
}
