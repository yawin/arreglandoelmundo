﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class credits : MonoBehaviour
{
    public Transform madmans;
    private float delay = 2.0f;
    private bool viewed = false;
    private bool ended = false;

    // Update is called once per frame
    void Update()
    {
        if(ended){Destroy(this);}

        delay -= Time.deltaTime;

        if(delay <= 0.0f)
        {
          if(!viewed)
          {
              if(madmans.position.y < 2.0f)
              {
                madmans.Translate(0, Time.deltaTime*18, 0);
              }
              else
              {
                madmans.position = new Vector3(6.0f, 2.0f, -5.0f);
                viewed = true;
                delay = 5.0f;
              }
          }
          else
          {
            MakeTransition.GoTo("StartGame", ExeControl.matList2);
          }
        }
    }
}
