﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameBullet : BasicBullet
{
    public int length;
    public float delay = 0.5f;

    private GameObject next = null;
    private FlameBullet nextBullet = null;
    public GameObject bullet;

    void Update()
    {
      if(next == null && length > 1)
      {
        if(delay > 0.0f)
        {
          delay -= Time.deltaTime;
        }
        else
        {
          next = Instantiate(bullet, transform.position + Vector3.up*5, transform.rotation);
          next.transform.parent = transform;
          nextBullet = next.GetComponent<FlameBullet>();
          nextBullet.length = length -1;
          nextBullet.delay = 0.5f;
        }
      }

      if(next)
      {
        float interpolation = speed * Time.deltaTime;
        Vector3 position = this.transform.position;
        position.y = Mathf.Lerp(next.transform.position.y, transform.position.y, interpolation);
        position.x = Mathf.Lerp(next.transform.position.x, transform.position.x, interpolation);
        next.transform.position = new Vector3(position.x, position.y, transform.position.z);
      }
    }

    public void BeDeceased()
    {
      Invoke("CallReferenced", 0.5f);
    }
    private void CallReferenced()
    {
      nextBullet.BeDeceased();
      Destroy(gameObject);
    }
}
