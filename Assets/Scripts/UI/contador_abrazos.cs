﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class contador_abrazos : MonoBehaviour
{
  private Text texto;
  public PlayerShootStrategy player = null;

  void Start()
  {
      texto = GetComponent<Text>();
  }

  // Update is called once per frame
  void Update()
  {
      if(player == null)
      {
        player = PlayerMoveStrategy.instance.GetComponent<PlayerShootStrategy>();
      }
      else if(player.weapon != null)
      {
        texto.text = "Abrazos: " + player.weapon.ammo_amount.ToString();
      }
  }
}
