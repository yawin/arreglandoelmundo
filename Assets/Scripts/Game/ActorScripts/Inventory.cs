﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<GameObject> weapon_list = new List<GameObject>();
    public int selectedWeapon = -1;

    public GameObject weapon;
    public Transform target;

    protected virtual void Update()
    {
      if(weapon_list.Count > 0 && selectedWeapon == -1)
      {
        changeWeapon(0);
      }
    }

    protected void changeWeapon(int w)
    {
      if(w >= 0 && w < weapon_list.Count)
      {
        if(weapon != null)
        {
          Destroy(weapon);
        }

        weapon = Instantiate(weapon_list[0], transform.position, transform.rotation);
        weapon.transform.parent = transform;
        weapon.GetComponent<WeaponTargeting>().target = target;
        
        if(gameObject.tag != "Player")
          weapon.GetComponent<WeaponTargeting>().ammo_amount = -1;

        selectedWeapon = w;
      }
    }
}
