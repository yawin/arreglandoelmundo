﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class autodead : MonoBehaviour
{
    public float live = 1.45f;

    void Update()
    {
        live -= Time.deltaTime;
        if(live <= 0)
        {
          Destroy(gameObject);
        }
    }
}
