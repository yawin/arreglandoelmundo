﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExeControl : MonoBehaviour
{
    public static List<Material> matList, matList2;

  	[RuntimeInitializeOnLoadMethod]
    public static void StartExeControl()
    {
      GameObject g = new GameObject();
      g.AddComponent<ExeControl>();
      g.transform.name = "ExeControl";
      Object.DontDestroyOnLoad(g);

      matList = new List<Material>();
      matList.Add(Resources.Load<Material>("Materials/Exit"));
      matList.Add(Resources.Load<Material>("Materials/Enter"));

      matList2 = new List<Material>();
      matList2.Add(Resources.Load<Material>("Materials/TransitionDiagonal"));
      matList2.Add(Resources.Load<Material>("Materials/TransitionDiagonal"));
    }

    // Update is called once per frame
    void Update()
    {
      if(Input.GetAxis("Cancel") != 0)
      {
        Application.Quit();
      }

      if(Input.GetAxis("Fire2") != 0 && SceneManager.GetActiveScene().name == "Prueba")
      {
        MakeTransition.GoTo(SceneManager.GetActiveScene().name, matList);
      }
    }
}
