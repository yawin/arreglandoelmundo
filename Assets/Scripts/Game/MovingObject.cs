using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The abstract keyword enables you to create classes and class members that are incomplete and must be implemented in a derived class.
public abstract class MovingObject : MonoBehaviour
{
    private BoxCollider2D boxCollider;
    private SpriteRenderer sr;
    protected Vector2 velocity;

    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        sr = GetComponent<SpriteRenderer>();
    }

    protected virtual void AttemptMove(Vector2 velocity)
    {
        if(velocity.x < 0)
        {
          sr.flipX = true;
        }
        else
        {
          sr.flipX = false;
        }

        transform.Translate(velocity * Time.deltaTime);

        /*Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, boxCollider.size, 0);

        foreach (Collider2D hit in hits)
        {
          if (hit == boxCollider)
          continue;

          ColliderDistance2D colliderDistance = hit.Distance(boxCollider);

          if (colliderDistance.isOverlapped)
          {
            transform.Translate(colliderDistance.pointA - colliderDistance.pointB);
          }
        }*/

        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y);
    }

    protected virtual void AttemptMoveInterpolated(Vector3 velocity)
    {
        transform.position = velocity;

        Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, boxCollider.size, 0);

        foreach (Collider2D hit in hits)
        {
          if (hit == boxCollider)
          continue;

          ColliderDistance2D colliderDistance = hit.Distance(boxCollider);

          if (colliderDistance.isOverlapped)
          {
            transform.Translate(colliderDistance.pointA - colliderDistance.pointB);
          }
        }

        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y);
    }
}
