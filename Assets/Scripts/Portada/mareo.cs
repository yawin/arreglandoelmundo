﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mareo : MonoBehaviour
{
    public float step, modif;

    void Start()
    {
      modif = Random.Range(-0.5f, 0.5f);
    }
    void Update()
    {
        step += (Time.deltaTime+modif)*0.5f;
        Vector2 paso = new Vector2((Mathf.Sin(step)*0.05f),(Mathf.Cos(step)*0.05f));
        transform.Translate(paso);
    }
}
