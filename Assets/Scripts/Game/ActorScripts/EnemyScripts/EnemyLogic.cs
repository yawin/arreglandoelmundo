﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class EnemyLogic : MovingObject
{
    public WeaponTargeting weapon;
    private Inventory inventory;
    private Animator animator;
    private int state;

    private float idleTime = -1;
    private float alertTime = 0;
    public float persecutionTime = 0;

    public float speed;

    public float viewDistance;
    public float alertTimeLimit;
    public float alarmDistance;
    public float shootDistance_min;
    public float shootDistance_max;
    public float persecutionTimeLimit;

    public PathCreator pathCreator;
    private float distanceTravelled;
    private SpriteRenderer sr;

    private bool notInPos = true;

    private Transform player;
    private Life playerLife;

    int oldState = -1;

    void Start()
    {
        GameObject tmp = GameObject.FindWithTag("Player");
        player = tmp.transform;
        playerLife = tmp.GetComponent<Life>();
        inventory = GetComponent<Inventory>();
        animator = GetComponent<Animator>();
        state = animator.GetInteger("State");
        sr = GetComponent<SpriteRenderer>();
        base.Start();
    }

    void Update()
    {
        if(animator.GetBool("Dead"))
        {
          GameController.instance.malos++;
          transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y+10);
          Destroy(GetComponent<Inventory>());
          Destroy(GetComponent<Rigidbody2D>());
          Destroy(GetComponent<BoxCollider2D>());
          for(int i = 0; i < transform.childCount; i++)
          {
            Destroy(transform.GetChild(i).gameObject);
          }
          Destroy(this);
        }

        if(oldState != animator.GetInteger("State"))
        {
          oldState = animator.GetInteger("State");
        }

        if(inventory.weapon != null && weapon != inventory.weapon.GetComponent<WeaponTargeting>())
        {
          weapon = inventory.weapon.GetComponent<WeaponTargeting>();
        }

        if(state != animator.GetInteger("State"))
        {
          state = animator.GetInteger("State");
        }

        switch(state)
        {
          case 0: //Idle
            CheckAlert();
            if(idleTime > 0.0f)
            {
              idleTime -= Time.deltaTime;
              if(idleTime <= 0.0f)
              {
                animator.SetInteger("State", 1);
              }
            }
            else
            {
              idleTime = Random.Range(0.0f, 5.0f);
            }
            break;

          case 1: //Apatrullando la ciudad
            CheckAlert();
            if(notInPos)
            {
              GoToPos();
            }
            else
            {
              Patrulla();
            }
            break;

          case 2: //Alerta
            if(playerLife.lives <= 0)
            {
              animator.SetInteger("State", 1);
              return;
            }

            //Mientras el player esté más de lejos de X
              if(Vector3.Distance(player.position, transform.position) > alarmDistance)
              {
                //El enemigo no se mueve
                  alertTime += Time.deltaTime;
                //Si pasa demasiado tiempo
                  if(alertTime >= alertTimeLimit)
                  {
                    //Vuelve a idle
                      animator.SetInteger("State", 0);
                  }
              }
            //Si el player se acerca más de X
              else
              {
                //El enemigo entra en estado Alarma
                  animator.SetInteger("State", 3);
                  weapon.target = player;
                  notInPos = true;

                //Y pone a los de su alrededor en estado de Alarma también
              }
            break;

          case 3: //Alarma
          case 4: //Ataque
          case 5: //ManteniendoPosicion
            if(playerLife.lives < 0)
            {
              animator.SetInteger("State", 0);
              return;
            }

            //Si el jugador está demasiado cerca
              if(Vector3.Distance(player.position, transform.position) < shootDistance_min)
              {
                persecutionTime = 0;

                //Toma distancia
                  MaintainDistance();
              }
            //Si está demasiado lejos
              else if(Vector3.Distance(player.position, transform.position) > shootDistance_max)
              {
                  persecutionTime += Time.deltaTime;
                //Lo persigue
                  MaintainDistance();

                //Si lo persigue demasiado tiempo
                  if(persecutionTime >= persecutionTimeLimit)
                  {
                    //Vuelve a alerta
                      animator.SetInteger("State", 2);
                      weapon.target = transform;
                      alertTime = 0;
                      persecutionTime = 0;
                  }
              }
            //Si no, dispara
            else
            {
              animator.SetInteger("State", 3);
              persecutionTime = 0;
              if(Random.Range(0.0f, 100.0f) < 100.0f*weapon.ShootingSpeed)
              {
                if(playerLife.lives > 0)
                {
                  weapon.Shoot();
                }
              }
            }
            break;
        }
    }

    private void MaintainDistance()
    {
      animator.SetInteger("State", 5);
      float interpolation = speed * Time.deltaTime;

      Vector3 position = this.transform.position;
      Vector3 target;

      if(Vector3.Distance(player.position, transform.position) < shootDistance_min)
      {
          target = (sr.flipX == true) ? Vector3.right : Vector3.left;
          target *= 2;
      }
      else if(Vector3.Distance(player.position, transform.position) > shootDistance_max)
      {
        target = player.position;
      }
      else
      {
        return;
      }

      position.y = Mathf.Lerp(this.transform.position.y, target.y, interpolation);
      position.x = Mathf.Lerp(this.transform.position.x, target.x, interpolation);

      AttemptMoveInterpolated(position);

      if(target.x < transform.position.x)
      {
        sr.flipX = true;
      }
      else
      {
        sr.flipX = false;
      }
    }

    private void CheckAlert()
    {
      if(playerLife.lives <= 0)
      {
        animator.SetInteger("State", 1);
        return;
      }

      if(Vector3.Distance(player.position, transform.position) < viewDistance)
      {
        alertTime = 0;
        animator.SetInteger("State", 2);
      }
    }

    private void GoToPos()
    {
      float interpolation = speed * Time.deltaTime;

      Vector3 position = this.transform.position;
      Vector3 target = pathCreator.path.GetPointAtDistance(distanceTravelled);
      position.y = Mathf.Lerp(this.transform.position.y, target.y, interpolation);
      position.x = Mathf.Lerp(this.transform.position.x, target.x, interpolation);

      AttemptMoveInterpolated(position);

      if(target.x < transform.position.x)
      {
        sr.flipX = true;
      }
      else
      {
        sr.flipX = false;
      }

      if((Vector3.Distance(new Vector3(target.x, target.y, transform.position.z), transform.position) < 0.25))
      {
        notInPos = false;
      }
    }

    private void Patrulla()
    {
      distanceTravelled += speed * Time.deltaTime;
      Vector3 newPos = pathCreator.path.GetPointAtDistance(distanceTravelled);
      if(newPos.x < transform.position.x)
      {
        sr.flipX = true;
      }
      else
      {
        sr.flipX = false;
      }

      AttemptMoveInterpolated(newPos);
    }
}
