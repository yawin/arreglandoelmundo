﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour
{
    public int lives = 1;
    private Animator animator;

    void Start()
    {
      animator = GetComponent<Animator>();
    }

    void Update()
    {
      //if()
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Bullet")
        {
          BasicBullet basicBullet = col.gameObject.GetComponent<BasicBullet>();

          if(basicBullet.faction == gameObject.tag)
          {
            return;
          }
          //Obtenemos daño de la bala
            lives -= basicBullet.damage;
          //Y restamos daño a la vida
            Destroy(col.gameObject);

            if(lives <= 0)
            {
              animator.SetBool("Dead", true);
            }
        }
    }
}
