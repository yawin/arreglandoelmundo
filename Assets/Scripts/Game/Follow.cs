using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour
{
    public GameObject objectToFollow;
    public float speed = 5.0f;

    void Update()
    {
        float interpolation = speed * Time.deltaTime;

        Vector3 position = this.transform.position;
        position.y = Mathf.Lerp(this.transform.position.y, objectToFollow.transform.position.y, interpolation);
        position.x = Mathf.Lerp(this.transform.position.x, objectToFollow.transform.position.x, interpolation);

        this.transform.position = new Vector3(position.x, position.y, -500-PlayerMoveStrategy.instance.transform.position.z);

        /*for(int i = 0; i < 16; i++)
        {
          if(Input.GetKeyDown("joystick 1 button " + i))
          {
            Debug.Log("[Tecla] joystick 1 button " + i);
          }
        }*/
    }
}
