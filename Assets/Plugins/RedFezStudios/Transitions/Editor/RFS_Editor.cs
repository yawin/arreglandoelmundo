﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class RFS_Editor : EditorWindow
{
	[MenuItem("Window/Red Fez Studios")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(RFS_Editor), false, "Red Fez Studios' plugin configuration");
	}
	
	void Update()
	{
		Repaint();
	}
	
	private bool pp;
	void OnGUI()
	{
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		
		pp = EditorGUILayout.Toggle("Mostrar durezas", pp);
		
		PlayerPrefs.SetInt("colVis", pp ? 1 : 0);
	}
}
