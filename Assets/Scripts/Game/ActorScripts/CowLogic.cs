﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class CowLogic : MovingObject
{
    public PathCreator pathCreator;
    private float distanceTravelled;
    private Animator animator;

    private bool notInPos = true;
    public float speed;
    private SpriteRenderer sr;
    private int state;

    public GameObject ammo;

    void Start()
    {
      animator = GetComponent<Animator>();
      state = animator.GetInteger("State");
      sr = GetComponent<SpriteRenderer>();
      speed += Random.Range(-0.2f, 0.2f);
      base.Start();
    }

    void Update()
    {
        if(animator.GetBool("Dead"))
        {
          if(Random.Range(0, 100) <= 25)
          {
            Instantiate(ammo, new Vector3(transform.position.x, transform.position.y, transform.position.z-1), transform.rotation);
          }

          GameController.instance.vaques++;
          transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y+100);
          Destroy(GetComponent<Rigidbody2D>());
          Destroy(GetComponent<BoxCollider2D>());
          Destroy(this);
        }

        if(notInPos)
        {
          GoToPos();
        }
        else
        {
          Patrulla();
        }
    }

    private void GoToPos()
    {
      float interpolation = speed * Time.deltaTime;

      Vector3 position = this.transform.position;
      Vector3 target = pathCreator.path.GetPointAtDistance(distanceTravelled);
      position.y = Mathf.Lerp(this.transform.position.y, target.y, interpolation);
      position.x = Mathf.Lerp(this.transform.position.x, target.x, interpolation);

      AttemptMoveInterpolated(position);

      if(target.x < transform.position.x)
      {
        sr.flipX = true;
      }
      else
      {
        sr.flipX = false;
      }

      if((Vector3.Distance(new Vector3(target.x, target.y, transform.position.z), transform.position) < 0.25))
      {
        notInPos = false;
      }
    }

    private void Patrulla()
    {
      distanceTravelled += speed * Time.deltaTime;
      Vector3 newPos = pathCreator.path.GetPointAtDistance(distanceTravelled);
      if(newPos.x < transform.position.x)
      {
        sr.flipX = true;
      }
      else
      {
        sr.flipX = false;
      }

      AttemptMoveInterpolated(newPos);
    }
}
