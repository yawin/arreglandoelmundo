﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponTargeting : MonoBehaviour
{
  public Transform target;
  public GameObject ammo;
  public int ammo_amount;
  public int max_ammo_amount = 25;
  public float ShootingSpeed;
  public string faction;
  private float angle = 0.0f;
  private SpriteRenderer sr;

  private Vector3 modifier;

  void Start()
  {
    //Corrección de altura
      transform.Translate(0.0f, 0.80f, 0.0f);
    //

    faction = transform.parent.tag;
    sr = GetComponent<SpriteRenderer>();
  }

  public virtual void Update()
  {
    if(transform.eulerAngles.z > 0 && transform.eulerAngles.z < 180)
    {
      sr.flipX = true;
    }
    else
    {
      sr.flipX = false;
    }

    if(target != null)
    {
      Vector3 relative = transform.InverseTransformPoint(target.position);
      angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
      transform.Rotate(0,0, -angle);
    }

  }

  public virtual void Shoot()
  {
      if(ammo_amount != 0)
      {
        ammo_amount--;

        GameObject tmp = Instantiate(ammo, transform.position, transform.rotation);
        tmp.GetComponent<Renderer>().enabled = false;
        tmp.GetComponent<BasicBullet>().faction = faction;

        SoundManager.PlayFX("tiro" + ((Random.Range(1,100)%4) + 1));
      }
      else
      {
        SoundManager.PlayFX("noammo");
      }
  }
}
