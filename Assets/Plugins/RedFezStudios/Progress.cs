﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Score
{
	public string keyCode;
	public int value;
	
	public Score(string _keyCode, int _value = 0)
	{
		keyCode = _keyCode;
		value = _value;
	}
}

public class Progress
{
	public static List<Score> score = new List<Score>();
	public static void addProgress(string keyCode, int value)
	{
		for(int i = 0; i < score.Count; i++)
		{
			if(score[i].keyCode == keyCode)
			{
				score[i].value += value;
				return;
			}
		}

		score.Add(new Score(keyCode, value));
	}
	public static int getProgress(string keyCode)
	{
		for(int i = 0; i < score.Count; i++)
		{
			if(score[i].keyCode == keyCode)
			{
				return score[i].value;
			}
		}

		return -1;
	}
	
	public static void Save(string savefile)
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/" + savefile);
		bf.Serialize(file, Progress.score);
		file.Close();
	}
	
	public static void Load(string loadfile)
	{
		if(File.Exists(Application.persistentDataPath + "/" + loadfile))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/" + loadfile, FileMode.Open);
			Progress.score = (List<Score>)bf.Deserialize(file);
			file.Close();
		}
	}
}
