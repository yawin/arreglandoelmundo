﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    public GameObject Caza1, Caza2, texto;
    public List<GameObject> explosiones;
    private bool transitioned = false;
    public float aparitionSpeed = 4.0f;
    public float aparitionDelay;
    private bool ended = false, texted = false;

    private float NecesitamosCazas = 1.0f;
    public List<Transform> izquierda;
    public List<Transform> derecha;
    public List<Transform> puntos;

    void Start()
    {
      SoundManager.PlaySong("America");
    }

    void Update()
    {
      if(Input.GetAxis("Fire2") != 0 && !transitioned)
      {
        MakeTransition.GoTo("Prueba", ExeControl.matList);
        transitioned = true;
      }

      if(ended)
      {
        if(NecesitamosCazas <= 0.0f)
        {
          DameCazas();
          NecesitamosCazas = Random.Range(3.0f, 12.0f);

          if(!texted)
          {
            texted = true;
            Instantiate(texto, new Vector3(0, 0, 2), transform.rotation);
            NecesitamosExplosiones();
          }
        }
        else
        {
          NecesitamosCazas -= Time.deltaTime;
          if(Random.Range(0, 1000) < 10)
          {
            NecesitamosExplosiones();
          }
        }
        return;
      }

      if(aparitionDelay > 0.0f)
      {
        aparitionDelay -= Time.deltaTime;
      }
      else if(transform.localScale.x < 1.33)
      {
        transform.localScale = new Vector3(transform.localScale.x + (1.33f*Time.deltaTime*aparitionSpeed), transform.localScale.y + (1.33f*Time.deltaTime*aparitionSpeed), 1.0f);
      }
      else
      {
        transform.localScale = new Vector3(1.33f, 1.33f, 1.0f);
        ended = true;
      }
    }

    private void NecesitamosExplosiones()
    {
      bool[] tmp = new bool[puntos.Count];
      for(int i = Random.Range(2, puntos.Count-1); i >= 0; i--)
      {
        int e;
        do
        {
          e = Random.Range(0, puntos.Count-1);
        } while (tmp[e]);

        tmp[e] = true;

        Instantiate(explosiones[Random.Range(1, 100) % explosiones.Count], puntos[e].position, puntos[e].rotation);
      }

    }

    private void DameCazas()
    {
      if(Random.Range(0, 11) % 2 == 0)
      {
        for(int i = Random.Range(0, izquierda.Count-1); i >= 0; i--)
        {
          Instantiate(Caza1, izquierda[i].position, izquierda[i].rotation);
        }
      }
      else
      {
        for(int i = Random.Range(0, derecha.Count-1); i >= 0; i--)
        {
          Instantiate(Caza2, derecha[i].position, derecha[i].rotation);
        }
      }
    }
}
