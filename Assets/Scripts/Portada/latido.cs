﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class latido : MonoBehaviour
{
    public float step;

    // Update is called once per frame
    void Update()
    {
      step += Time.deltaTime*2;
      float paso = transform.localScale.x + (Mathf.Sin(step)*0.05f);
      transform.localScale = new Vector3(paso, paso, paso);
    }
}
