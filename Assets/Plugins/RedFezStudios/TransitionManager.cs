﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionManager : MonoBehaviour
{
	private GameObject camara;
	private List<Material> inout;

	public static TransitionManager instance = null;
	private string target = "";

	private float delay = 0.0f, actualTime = 0.0f;
	private bool isFading = false;

	public void PlayTransition(string _target, float _delay, List<Material> _inout)
	{
		if(null != instance)
		{
			Destroy(gameObject);
		}

		instance = this;

		target = _target;
		delay = _delay;
		isFading = true;

		camara = GameObject.FindWithTag("MainCamera");
		camara.AddComponent<SimpleBlit>();
		camara.GetComponent<SimpleBlit>().TransitionMaterial = _inout[0];
		inout = _inout;
	}

	void Update()
	{
		if(isFading)
		{
			if(actualTime < delay * 0.5)
			{
				actualTime += Time.deltaTime;
			}
			else if(actualTime >= delay * 0.5 && "" != target)
			{
				SceneManager.LoadScene(target);
				target = "";

				camara = null;
			}
			else if(actualTime <= delay && actualTime >= delay * 0.5)
			{
				if(null == camara)
				{
					camara = GameObject.FindWithTag("MainCamera");
					camara.AddComponent<SimpleBlit>();
					camara.GetComponent<SimpleBlit>().TransitionMaterial = inout[1];
				}

				actualTime += Time.deltaTime;
			}
			else
			{
				instance = null;
				Destroy(camara.GetComponent<SimpleBlit>());
				Destroy(gameObject);
			}
		}
	}

	void OnGUI()
	{
		if(isFading)
		{
			float alpha = 1.0f;
			if(actualTime < delay*0.5)
			{
				alpha = actualTime / (delay*0.5f);
				inout[0].SetFloat("_Cutoff", alpha);
			}
			else
			{
				float aux = actualTime - delay*0.5f;
				alpha = aux / (delay*0.5f);
				alpha = 1.0f - alpha;
				inout[1].SetFloat("_Cutoff", alpha);
			}
		}
	}
}

public class MakeTransition
{
	public static void GoTo(int target, List<Material> _inout)
	{
		GoTo(target, 3.0f, _inout);
	}
	public static void GoTo(int target, float delay, List<Material> _inout)
	{
		string targetScene = SceneUtility.GetScenePathByBuildIndex(target);
		string sceneName = targetScene.Substring(0, targetScene.Length - 6).Substring(targetScene.LastIndexOf('/') + 1);
		GoTo(sceneName, 3.0f, _inout);
	}

	public static void GoTo(string target, List<Material> _inout)
	{
		GoTo(target, 3.0f, _inout);
	}
	public static void GoTo(string target, float delay, List<Material> _inout)
	{
		if(TransitionManager.instance != null){return;}

		List<Material> inout_materials = _inout;
		if(inout_materials.Count == 1){inout_materials.Add(inout_materials[0]);}

		GameObject f = new GameObject();
		f.transform.name = "TransitionManager";
		f.AddComponent<TransitionManager>();
		f.GetComponent<TransitionManager>().PlayTransition(target, delay, _inout);
		Object.DontDestroyOnLoad(f);
	}
}
