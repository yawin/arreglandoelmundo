﻿using System.Collections;
using SimpleJSON;
using UnityEngine;

public class Localization : MonoBehaviour
{
	public static JSONNode Locale;
	private static string lang = "Spanish";

	public static string getLang()
	{
		return lang;
	}
	public static void setLang(string _lang)
	{
		//Buscamos si el idioma está disponible
		TextAsset tF;

		//Si está disponible
			if(tF = Resources.Load<TextAsset>("Locale/" + _lang))
			{
				lang = _lang;
			}
		//Si no
			else
			{
				//Asignamos el idioma por defecto (Spanish)
					tF = Resources.Load<TextAsset>("Locale/Spanish");
					lang = "Spanish";
			}

		//Guardamos en "playerprefs" el idioma seleccionado
			PlayerPrefs.SetString("Locale", lang);

		Locale = JSON.Parse(tF.text);
		Debug.Log("Locale setted as " + lang);
	}

	public static void setLocalLanguage()
	{
		setLang(Application.systemLanguage.ToString());
	}

	[RuntimeInitializeOnLoadMethod]
	public static void StartApi()
	{
		if("" != PlayerPrefs.GetString("Locale"))
		{
			setLang(PlayerPrefs.GetString("Locale"));
		}
		else
		{
			setLocalLanguage();
		}
	}

	public static string getText(string keyCode)
	{
		return Localization.Locale[keyCode].Value;
	}
}
