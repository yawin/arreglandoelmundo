﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveStrategy : MovingObject
{
    public static GameObject instance;

    [SerializeField, Tooltip("Max speed, in units per second, that the character moves.")]
    float speed = 9;
    [SerializeField, Tooltip("Acceleration while grounded.")]
    float walkAcceleration = 75;
    [SerializeField, Tooltip("Deceleration applied when character is grounded and not attempting to move.")]
    float groundDeceleration = 70;

    private Animator animator;
    private Life life;

    void Start()
    {
      instance = gameObject;
      animator = GetComponent<Animator>();
      life = GetComponent<Life>();
      base.Start ();
    }

    private void Update ()
    {
      if(animator.GetBool("Dead"))
      {
        Destroy(GetComponent<PlayerInventory>());
        Destroy(GetComponent<Rigidbody2D>());
        Destroy(GetComponent<BoxCollider2D>());

        for(int i = 0; i < transform.childCount; i++)
        {
          Destroy(transform.GetChild(i).gameObject);
        }
        Destroy(this);
      }

      int horizontal = 0;
      int vertical = 0;

      horizontal = (int) (Input.GetAxisRaw ("Horizontal"));
      vertical = (int) (Input.GetAxisRaw ("Vertical"));

      if(horizontal != 0 || vertical != 0)
      {
        if(animator.GetInteger("State") != 1)
          animator.SetInteger("State", 1);

        if (horizontal != 0)
        {
          //transform.localScale = new Vector3((horizontal < 0) ? -0.75f : 0.75f, transform.localScale.y, transform.localScale.z);

          velocity.x = Mathf.MoveTowards(velocity.x, speed * horizontal, walkAcceleration * Time.deltaTime);
        }
        else
        {
          velocity.x = Mathf.MoveTowards(velocity.x, 0, groundDeceleration * Time.deltaTime);
        }

        if (vertical != 0)
        {
          velocity.y = Mathf.MoveTowards(velocity.y, speed * vertical, walkAcceleration * Time.deltaTime);
        }
        else
        {
          velocity.y = Mathf.MoveTowards(velocity.y, 0, groundDeceleration * Time.deltaTime);
        }

        AttemptMove(velocity);
      }
      else
      {
        if(animator.GetInteger("State") != 0)
          animator.SetInteger("State", 0);
      }
    }
}
