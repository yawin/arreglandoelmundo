﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ImageTools.Core;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public int malos = 0, vaques = 0;
    public List<GameObject> decorationList;
    public List<GameObject> entityList;
    public GameObject ammo;

    private Life playerLives;
    private SpriteRenderer sr;
    public float aparitionSpeed = 4.0f;
    public float aparitionDelay = 0.75f;
    private bool ended = false;

    private int rango = 10;
    private Vector3 lastPos;

    private Dictionary<string, bool> mapa;
    public int octavas;
    public int densidad;
    private PerlinNoise perlinNoise;

    void Start()
    {
        instance = this;
        SoundManager.PlaySong("Bird");
        GameObject tmp = GameObject.FindWithTag("Player");
        playerLives = tmp.GetComponent<Life>();
        sr = GetComponent<SpriteRenderer>();

        transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
        mapa = new Dictionary<string, bool>();
        perlinNoise = new PerlinNoise(99);

        IniciaMundo();
    }

    void Update()
    {
      if(ended)
      {
        return;
      }

      ActualizaMundo();

      if(playerLives.lives <= 0)
      {
        sr.enabled = true;

        if(aparitionDelay > 0.0f)
        {
          aparitionDelay -= Time.deltaTime;
        }
        else if(transform.localScale.x < 1.52)
        {
          SoundManager.PlaySong("Fallo");
          transform.localScale = new Vector3(transform.localScale.x + (1.52f*Time.deltaTime*aparitionSpeed), transform.localScale.y + (1.55f*Time.deltaTime*aparitionSpeed), 1.0f);
        }
        else
        {
          transform.localScale = new Vector3(1.52f, 1.55f, 1.0f);
          ended = true;
        }
      }
    }

    void IniciaMundo()
    {
      lastPos = PlayerMoveStrategy.instance.transform.position;
      GeneraMundo(new int[]{(int)PlayerMoveStrategy.instance.transform.position.x - 15*4,
                   (int)PlayerMoveStrategy.instance.transform.position.y - 15*4,
                   (int)PlayerMoveStrategy.instance.transform.position.x + 15*4,
                   (int)PlayerMoveStrategy.instance.transform.position.y + 15*4});
    }

    void ActualizaMundo()
    {
      Vector2 newPos = PlayerMoveStrategy.instance.transform.position;
      int x = (int) newPos.x;
      int y = (int) newPos.y;

      x = x % 15;
      y = y % 15;

      int[] chunk_limits = {
        (int)newPos.x - x,
        (int)newPos.y - y,
        (int)newPos.x + (15 - x),
        (int)newPos.y + (15 - y)
      };

      if(lastPos.x < newPos.x)
      {
        chunk_limits[2] += 30;
      }
      else if(lastPos.x > newPos.x)
      {
        chunk_limits[0] -= 30;
      }

      if(lastPos.y < newPos.y)
      {
        chunk_limits[3] += 30;
      }
      else if(lastPos.y > newPos.y)
      {
        chunk_limits[1] -= 30;
      }

      GeneraMundo(chunk_limits);

      lastPos = newPos;
    }

    private double Min(double a, double b)
    {
      return (a < b) ? a : b;
    }
    private double Max(double a, double b)
    {
      return (a > b) ? a : b;
    }

    void GeneraMundo(int[] chunk_limits)
    {
      int min_x = chunk_limits[0];
      int min_y = chunk_limits[1];
      int max_x = chunk_limits[2];
      int max_y = chunk_limits[3];

      for(int i = min_x; i < max_x; i++)
      {
        for(int j = min_y; j < max_y; j++)
        {
          if(!mapa.ContainsKey(i + "-" + j))
          {
            mapa[i + "-" + j] = true;

            double v = 0.0;

            for(int o = 1; o <= octavas; o++)
            {
              v += perlinNoise.Noise(2*o*i/15, 2*o*j/15, -0.5);
            }

            //v = Min(1, Max(0, v));
            int b = (int)(v*255);

            if(b < densidad && b > -densidad)
            {
              float r = Random.Range(0.0f, 1000.0f);
              if(r < 900.0f)
              {
                Instantiate(decorationList[Random.Range(0, 4)], new Vector3(i, j, 0.0f), transform.rotation);
              }
              else if(r < 915.0f)
              {
                Instantiate(ammo, new Vector3(i, j, 0.0f), transform.rotation);
              }
              else if(r < 965.0f)
              {
                Instantiate(entityList[0], new Vector3(i, j, 0.0f), transform.rotation);
              }
              else
              {
                Instantiate(entityList[1], new Vector3(i, j, 0.0f), transform.rotation);
              }
            }
          }
        }
      }
    }
}
