﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScopeLogic : MonoBehaviour
{
  [SerializeField, Tooltip("Max speed, in units per second, that the character moves.")]
  float speed = 9;

  [SerializeField, Tooltip("Aceleración de movimiento")]
  float acceleration = 80;

  [SerializeField, Tooltip("Distancia límite")]
  float limit = 2;

  private Vector2 velocity;
  private float dist;
  public Transform weapon;

  void Start()
  {
      //Corrección de altura
        transform.Translate(1.0f, 0.80f, 0.0f);
      //
  }

  void Update()
  {
    if(!weapon){return;}
    
    int horizontal = 0;
    int vertical = 0;

    horizontal = (int) (Input.GetAxisRaw ("Horizontal_alt"));
    vertical = (int) (Input.GetAxisRaw ("Vertical_alt"));

    if(horizontal != 0 || vertical != 0)
    {
      if (horizontal != 0)
      {
        velocity.x = speed * horizontal * Time.deltaTime;
      }

      if (vertical != 0)
      {
        velocity.y = speed * vertical * Time.deltaTime;
      }

      transform.Translate(velocity);

      Vector3 pointA, pointB;
      if(velocity.x > 0)
      {
        pointB = transform.position;
        pointA = weapon.position;
      }
      else
      {
        pointA = transform.position;
        pointB = weapon.position;
      }

      dist = Mathf.Sqrt(Mathf.Pow(pointB.x - pointA.x, 2));

      if(dist > limit)
      {
        if(velocity.x > 0)
          transform.Translate(limit - dist, 0, 0);
        else
          transform.Translate(-(limit - dist), 0, 0);
      }

      if(velocity.y > 0)
      {
        pointB = transform.position;
        pointA = weapon.position;
      }
      else
      {
        pointA = transform.position;
        pointB = weapon.position;
      }

      dist = Mathf.Sqrt(Mathf.Pow(pointB.y - pointA.y, 2));

      if(dist > limit)
      {
        if(velocity.y > 0)
          transform.Translate(0, limit - dist, 0);
        else
          transform.Translate(0, -(limit - dist), 0);
      }
    }
  }
}
