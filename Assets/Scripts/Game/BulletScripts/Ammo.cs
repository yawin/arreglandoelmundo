﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col)
    {
      if(col.tag == "Ammo")
      {
        WeaponTargeting weapon = PlayerMoveStrategy.instance.GetComponent<PlayerShootStrategy>().weapon;
        weapon.ammo_amount = (weapon.ammo_amount + 10 < weapon.max_ammo_amount) ? weapon.ammo_amount + 10 : weapon.max_ammo_amount;
        SoundManager.PlayFX("reload");
        Destroy(col.gameObject);
      }
    }
}
