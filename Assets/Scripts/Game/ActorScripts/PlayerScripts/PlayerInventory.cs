﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : Inventory
{
    private PlayerShootStrategy pss;
    void Start()
    {
      pss = GetComponent<PlayerShootStrategy>();
    }
    void Update()
    {
      if(pss.scope != null && target == null)
      {
        target = pss.scope.transform;
      }

      base.Update();
    }
}
