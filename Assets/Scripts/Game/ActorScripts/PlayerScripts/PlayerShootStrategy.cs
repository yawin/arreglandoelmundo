﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShootStrategy : MonoBehaviour
{
    public GameObject scopePrefab;
    public GameObject scope;
    public WeaponTargeting weapon;
    private PlayerInventory pI;

    private float fired = 0.0f;
    private Life life;

    void Start()
    {
      scope = Instantiate(scopePrefab, transform.position, transform.rotation);
      scope.transform.parent = transform;
      pI = GetComponent<PlayerInventory>();
      life = GetComponent<Life>();
    }

    // Update is called once per frame
    void Update()
    {
      if(life.lives < 0)
      {
        Destroy(this);
      }

      if(pI.weapon != null && weapon != pI.weapon.GetComponent<WeaponTargeting>())
      {
        scope.GetComponent<ScopeLogic>().weapon = pI.weapon.transform;
        weapon = pI.weapon.GetComponent<WeaponTargeting>();
      }

      if(Input.GetAxis("Fire1") > 0)
      {
        if(fired <= 0.0)
        {
          weapon.Shoot();
          fired = weapon.ShootingSpeed;
        }
        else
        {
          fired -= Time.deltaTime;
        }
      }
      else
      {
        fired = 0;
      }
    }
}
